################################################################################
###Nematolos - RNAseq
################################################################################

    ###Targetfile
    targets <- mutate(targetfile) %>%
               mutate(Treatment=gsub("-","mock",Treatment,fixed = TRUE),Treatment=gsub("+","H.schachtii",Treatment,fixed = TRUE)) %>%
               rename(replicate=Sample_nr,strain=Plant_line,timepoint=hpi,treatment=Treatment)



################################################################################
###Arabidopsis
################################################################################


    ###Load data
        RNAseq <- getwd()
        setwd(paste(RNAseq,"/quantified/",sep=""))
        
        tmp <- read.csv2("TAIR10_TPM_counts.csv") %>%
               gather(key=sample,value=TPM,-transcript_name) %>%
               merge(targets,by.x=2,by.y=1) %>%
               mutate(TPM=as.numeric(as.character(unlist(TPM)))) %>%
               mutate(SampleID=paste(strain,timepoint,treatment,replicate,sep=":"))
        
        
        
        ###Check detection of reads over samples
        ###exlored; but did not yield very highly expressed genes, probably mostly noise
            # data.plot <- group_by(tmp,transcript_name, treatment !="mock") %>%
            #              summarise(n_counts=sum(TPM>0)) %>%  
            #              group_by(transcript_name) %>%
            #              filter(sum(n_counts==0)==1) %>%
            #              data.frame() %>%
            #              filter(!duplicated(transcript_name))
            # 
            # data.plot2 <- filter(tmp,transcript_name %in% data.plot$transcript_name) %>%
            #               group_by(transcript_name, treatment !="mock") %>%
            #               mutate(n_counts=sum(TPM>0)/length(TPM)) %>% 
            #               group_by(transcript_name) %>%
            #               mutate(n_counts=max(n_counts)) %>%
            #               data.frame() %>%
            #               filter(n_counts==1)
            # 
            # ggplot(data.plot2,aes(x=timepoint,y=log(TPM),colour=strain)) +
            # geom_point() + facet_wrap(~transcript_name)

        
        
        ###Check detection of reads over samples
            data.plot <- group_by(tmp,transcript_name) %>%
                         summarise(n_counts=sum(TPM>0))
            
            p1 <- ggplot(data.plot,aes(x=n_counts)) +
                  geom_histogram(binwidth=1) + labs(title="Read coverage")
            
        ###Check TPM distribution reads detected in all samples 
            data.plot <- group_by(tmp,transcript_name) %>%
                         mutate(n_counts=sum(TPM>0)) %>%
                         data.frame() %>%
                         filter(n_counts==max(n_counts))
            
            p2 <- ggplot(data.plot,aes(x=TPM)) +
                  geom_histogram(binwidth=1) + coord_cartesian(xlim=c(0,1000)) + labs(title="TPM")
                     
        ###normalize
            ara_rna <- group_by(tmp,transcript_name) %>%
                       mutate(n_counts=sum(TPM>0)) %>%
                       data.frame() %>%
                       filter(n_counts==max(n_counts)) %>%
                       mutate(TPM_log=log2(TPM+1)) %>%
                       group_by(transcript_name) %>%
                       mutate(TPM_ratio_mean=log2(TPM/mean(TPM)),TPM_zscore=((TPM)-mean(TPM))/sd(TPM)) %>%
                       data.frame()
            
        ###plot distribution transformations
            p3 <- ggplot(ara_rna,aes(x=TPM_zscore)) +
                  geom_histogram(binwidth=0.1) + labs(title="Post-norm TPM z-score")
    
            p4 <- ggplot(ara_rna,aes(x=TPM_log)) +
                  geom_histogram(binwidth=0.1) + labs(title="Post-norm TPM log2")
            
            p5 <- ggplot(ara_rna,aes(x=TPM_ratio_mean)) +
                  geom_histogram(binwidth=0.1) + labs(title="Post-norm TPM ratio with mean")
                
        ###Stats for paper/ reporting
            ###unique genes 28600
            length(unique(ara_rna$transcript_name))

            ###unique genes pre filter 48359
            dim(tmp)[1]/72
        
    ###Load gene annotation
        gtf <- read.delim("assemblies_and_annotations/Arabidopsis_thaliana.TAIR10.44.gff3",header=FALSE, col.names=c("chromosome","database","type","type_bp_start","type_bp_end","dot","strand","splicevariant","identifiers"))

        ara.id <- filter(gtf,type=="mRNA") %>%
                  separate_rows(identifiers,sep = ";") %>%
                  separate(identifiers,into=c("type","identifiers"),sep="=") %>%
                  filter(type=="ID") %>%
                  select(-type) %>%
                  mutate(Parent=gsub("transcript:","",identifiers)) %>%
                  mutate(Parent=gsub("\\..","",Parent))
               
        list.data.ara <- ara_rna
        
        setwd(workwd)
            save(list.data.ara,file="./RNAseq/obj_list.data.ara.Rdata")
            save(ara.id,file="./RNAseq/obj_ara.id.Rdata")
            
            write.xlsx(ara.id,file="./RNAseq/Ara_genes.xlsx")
            write.table(list.data.ara,file="./RNAseq/ara_normalized_RNAseq.txt",sep="\t",quote=F)
            
            pdf("./RNAseq/Arabidopsis_quality_check.pdf",width=8,height=5)
                print(p1); print(p2); print(p3); print(p4); print(p5)
            dev.off()

            

################################################################################
###H. schachtii
################################################################################
            
    ###Load data
        setwd(paste(RNAseq,"/quantified/",sep=""))
        
        tmp <- read.csv2("HS_Cam1.2_TPM_counts.csv") %>%
               gather(key=sample,value=TPM,-transcript_name) %>%
               merge(targets,by.x=2,by.y=1) %>%
               select(-sample) %>%
               mutate(TPM=as.numeric(as.character(unlist(TPM)))) %>%
               mutate(SampleID=paste(strain,timepoint,treatment,replicate,sep=":"))
                
        ###Check detection of reads over samples
            data.plot <- group_by(tmp,transcript_name,treatment) %>%
                         summarise(n_counts=sum(TPM>0))
            
            p1 <- ggplot(data.plot,aes(x=n_counts)) +
                  geom_histogram(binwidth=1) + facet_grid(~treatment) + labs(title="Read coverage")
            
        
        ###Check TPM distribution reads detected in at least 50% of infected samples (36/2 = 18)
            data.plot <- filter(tmp,treatment=="H.schachtii") %>%
                         group_by(transcript_name) %>%
                         mutate(n_counts=sum(TPM>0)) %>%
                         data.frame() %>%
                         filter(n_counts>=18)
            
            p2 <- ggplot(data.plot,aes(x=TPM)) +
                  geom_histogram(binwidth=1) + coord_cartesian(xlim=c(0,250)) + labs(title="TPM")
                     
        
        het_rna <- filter(tmp,treatment=="H.schachtii") %>%
                   group_by(transcript_name) %>%
                   mutate(n_counts=sum(TPM>0)) %>%
                   data.frame() %>%
                   filter(n_counts>=18) %>%
                   mutate(TPM_log=log2(TPM+1)) %>%
                   group_by(transcript_name) %>%
                   mutate(TPM_ratio_mean=log2(TPM/mean(TPM)),TPM_zscore=((TPM)-mean(TPM))/sd(TPM)) %>%
                   mutate(TPM_ratio_mean=ifelse(TPM_ratio_mean==-Inf,0,TPM_ratio_mean)) %>%
                   data.frame()
            
        
        p3 <- ggplot(het_rna,aes(x=TPM_zscore)) +
              geom_histogram(binwidth=0.1) + labs(title="Post-norm TPM z-score")

        p4 <- ggplot(het_rna,aes(x=TPM_log)) +
              geom_histogram(binwidth=0.1) + labs(title="Post-norm TPM log2")
        
        p5 <- ggplot(het_rna,aes(x=TPM_ratio_mean)) +
              geom_histogram(binwidth=0.1) + labs(title="Post-norm TPM ratio with mean")
                
        ###Stats for paper/ reporting
            ###unique genes
            length(unique(het_rna$transcript_name))
            
            ###unique genes pre filter
            dim(tmp)[1]/72
        
        
    ###Load gene annotation
        gtf <- read.delim("assemblies_and_annotations/H_sch_gene_calls_v1_CP.gff",header=FALSE, col.names=c("chromosome","database","type","type_bp_start","type_bp_end","dot","strand","splicevariant","identifiers"))
    
        het.id <- filter(gtf,type=="mRNA")
        
        list.data.het <- het_rna
        
        setwd(workwd)
            save(list.data.het,file="./RNAseq/obj_list.data.het.Rdata")
            save(het.id,file="./RNAseq/obj_het.id.Rdata")
            
            write.table(list.data.het,file="./RNAseq/H.schachtii_normalized_RNAseq.txt",sep="\t",quote=F)
            
            pdf("./RNAseq/schachtii_quality_check.pdf",width=8,height=5)
                print(p1); print(p2); print(p3); print(p4); print(p5)
            dev.off()
   





